/*****************************************************************************
 * avaudio_filters.c : 
 *****************************************************************************
 * Copyright © 2022
 *
 * Authors: Razdutt Sarnaik
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include "avaudio.h"


/*****************************************************************************
 * Local prototypes
 *****************************************************************************/

typedef struct 
{
    float tremolo_mfreq;
    float tremolo_mdepth;

    vlc_mutex_t lock;
}tremolo_para_sys_t;

typedef struct 
{
    float vibrato_mfreq;
    float vibrato_mdepth;

    vlc_mutex_t lock;
}vibrato_para_sys_t;

tremolo_para_sys_t *p_sys_tremolo;
vibrato_para_sys_t *p_sys_vibrato;


static int Open( vlc_object_t *p_this );
static int Open_adelay( vlc_object_t *p_this );
static int Open_aecho( vlc_object_t *p_this );
static int Open_anull( vlc_object_t *p_this );
static int Open_apulsator( vlc_object_t *p_this );
static int Open_asoftclip( vlc_object_t *p_this );
static int Open_chorus( vlc_object_t *p_this );
static int Open_extrastereo( vlc_object_t *p_this );
static int Open_haas( vlc_object_t *p_this );
static int Open_tremolo( vlc_object_t *p_this );
static int Open_vibrato( vlc_object_t *p_this );
static int Open_firequalizer( vlc_object_t *p_this );


static int paramCallback_tremolo( vlc_object_t *, char const *, vlc_value_t,
                                  vlc_value_t, void * );
static int paramCallback_vibrato( vlc_object_t *, char const *, vlc_value_t,
                                  vlc_value_t, void * );


static char* string_generate_tremolo(filter_t*);
static char* string_generate_vibrato(filter_t*);


/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
#define INDENT_SPACE  "         "
#define INDENT_SPACE2 "    "

//delay
#define ADELAY_TEXT N_( "adelay" )
#define ADELAY_LONGTEXT N_( "Delay one or more audio channels" )

#define DELAYS_TEXT N_( INDENT_SPACE "delays" )
#define DELAYS_LONGTEXT N_( "Set list of delays in milliseconds for each channel" \
                " separated by '|'. Unused delays will be silently ignored." \
                " If number of given delays is smaller than number of channels all" \
                " remaining channels will not be delayed." )

//aecho
#define AECHO_TEXT N_( "aecho" )
#define AECHO_LONGTEXT N_( "Echo effect" )

#define AECHO_IN_GAIN_TEXT N_( INDENT_SPACE "in_gain" )
#define AECHO_IN_GAIN_LONGTEXT N_( "Set the in_gain." )

#define AECHO_OUT_GAIN_TEXT N_( INDENT_SPACE "out_gain" )
#define AECHO_OUT_GAIN_LONGTEXT N_( "Set the out_gain (reflected)." )

#define AECHO_DELAY_TEXT N_( INDENT_SPACE "delays" )
#define AECHO_DELAY_LONGTEXT N_( "Set list of time intervals in "\
                    "milliseconds between original signal and "\
                    "reflections separated by '|'. Allowed "\
                    "range for each delay is (0 - 90000.0]" )

#define AECHO_DECAY_TEXT N_( INDENT_SPACE "decays" )
#define AECHO_DECAY_LONGTEXT N_( "Set list of loudness of reflected "\
                    "signals separated by '|'. Allowed range for "\
                    "each decay is (0 - 1.0]. Default is 0.5." )

//anull
#define ANULL_TEXT N_( "anull" )
#define ANULL_LONGTEXT N_( "null : input passed to output unchanged" )

//apulsator
#define NB_MODES 5
static const char *const apulsator_mode_list[NB_MODES] = {
    "sine", "triangle", "square", "sawup", "sawdown"
};
static const char *const apulsator_mode_list_text[NB_MODES] = {
    N_("sine"), N_("triangle"), N_("square"),N_("sawup"),N_("sawdown")
};


#define NB_TIMINGS 3
static const char *const apulsator_timing_list[NB_TIMINGS] = {
    "bpm", "ms","hz"
};
static const char *const apulsator_timing_list_text[NB_TIMINGS] = {
    N_("bpm"), N_("ms"), N_("hz")
};

#define APULSATOR_TEXT N_( "apulsator" )
#define APULSATOR_LONGTEXT N_( "Pulsator" )

#define APULSATOR_LEVEL_IN_TEXT N_( INDENT_SPACE "level_in" )
#define APULSATOR_LEVEL_IN_LONGTEXT N_( "Set input gain. By default it is 1. Range is [0.015625 - 64]." )

#define APULSATOR_LEVEL_OUT_TEXT N_( INDENT_SPACE "level_out" )
#define APULSATOR_LEVEL_OUT_LONGTEXT N_( "Set output gain. By default it is 1. Range is [0.015625 - 64]." )

#define MODE_TEXT N_( INDENT_SPACE "mode" )
#define MODE_LONGTEXT N_( "Set waveform shape the LFO will use. Default is sine." )

#define AMOUNT_TEXT N_( INDENT_SPACE "amount" )
#define AMOUNT_LONGTEXT N_( "Set modulation. Define how much of original signal is affected by the LFO." )

#define OFFSET_L_TEXT N_( INDENT_SPACE "offset_l" )
#define OFFSET_L_LONGTEXT N_( "Set left channel offset. Default is 0. Allowed range is [0 - 1]." )

#define OFFSET_R_TEXT N_( INDENT_SPACE "offset_r" )
#define OFFSET_R_LONGTEXT N_( "Set right channel offset. Default is 0.5. Allowed range is [0 - 1]." )

#define WIDTH_TEXT N_( INDENT_SPACE "width" )
#define WIDTH_LONGTEXT N_( "Set pulse width. Default is 1. Allowed range is [0 - 2]." )

#define TIMING_TEXT N_( INDENT_SPACE "timing" )
#define TIMING_LONGTEXT N_( "Set possible timing mode. Default is hz." )

#define BPM_TEXT N_( INDENT_SPACE "bpm" )
#define BPM_LONGTEXT N_( "Set bpm. Default is 120. Allowed range is [30 - 300]." \
                " Only used if timing is set to bpm." )

#define MS_TEXT N_( INDENT_SPACE "ms" )
#define MS_LONGTEXT N_( "Set ms. Default is 500. Allowed range is [10 - 2000]." \
                " Only used if timing is set to ms." )

#define HZ_TEXT N_( INDENT_SPACE "hz" )
#define HZ_LONGTEXT N_( "Set frequency in Hz. Default is 2. Allowed range is [0.01 - 100]." \
                " Only used if timing is set to hz." )

//asoftclip
#define NB_TYPES 9
static const char *const softclip_type_list[NB_TYPES] = {
    "hard", "tanh", "atan", "cubic", "exp", "alg",
    "quintic", "sin","erf"
};
static const char *const softclip_type_list_text[NB_TYPES] = {
    N_("hard"), N_("tanh"), N_("atan"),
    N_("cubic"),N_("exp"), N_("alg"),
    N_("quintic"), N_("sin"),N_("erf")
};

#define ASOFTCLIP_TEXT N_( "asoftclip" )
#define ASOFTCLIP_LONGTEXT N_( "Set type of soft-clipping." )

#define TYPE_TEXT N_( INDENT_SPACE "type" )
#define TYPE_LONGTEXT N_( "Set type of soft-clipping." )

#define THRESHOLD_TEXT N_( INDENT_SPACE "threshold" )
#define THRESHOLD_LONGTEXT N_( "Set threshold from where to start clipping." \
                " Default value is 0dB or 1." )

//chorus
#define CHORUS_TEXT N_( "chorus" )
#define CHORUS_LONGTEXT N_( "Chorus effect" )

#define IN_GAIN_TEXT N_( INDENT_SPACE "in_gain" )
#define IN_GAIN_LONGTEXT N_( "Set the in_gain." )

#define OUT_GAIN_TEXT N_( INDENT_SPACE "out_gain" )
#define OUT_GAIN_LONGTEXT N_( "Set the out_gain (reflected)." )

#define DELAY_TEXT N_( INDENT_SPACE "delays" )
#define DELAY_LONGTEXT N_( "Set the delays in"\
                " milliseconds separated by '|'."\
                " Typical delay is arround 40ms to 60ms."\
                " Allowed "\
                " range for each delay is (0 - 90000.0]" )

#define DECAY_TEXT N_( INDENT_SPACE "decays" )
#define DECAY_LONGTEXT N_( "Set deacays in"\
                " signals separated by '|'. Allowed range for"\
                " each decay is (0 - 1.0]. Default is 0.5." )

#define SPEED_TEXT N_( INDENT_SPACE "speeds" )
#define SPEED_LONGTEXT N_( "Set speeds for"\
                " signals separated by '|'. Allowed range for"\
                " each decay is (0 - 1.0]. Default is 0.5." )

#define DEPTH_TEXT N_( INDENT_SPACE "depths" )
#define DEPTH_LONGTEXT N_( "Set deacays in"\
                " signals separated by '|'. Allowed range for"\
                " each decay is (0 - 10]. Default is 2." )

//extrastereo
#define EXTRASTEREO_TEXT N_( "extrastereo" )
#define EXTRASTEREO_LONGTEXT N_( "Linearly increases the difference between left and right" \
                " channels which adds some sort of 'live' effect to playback." )

#define EXTRASTEREO_M_TEXT N_( INDENT_SPACE "difference coefficient" )
#define EXTRASTEREO_M_LONGTEXT N_( "Sets the difference coefficient" \
                " (default: 2.5). 0.0 means mono sound (average of both" \
                " channels), with 1.0 sound will be unchanged, with -1.0" \
                " left and right channels will be swapped." )

#define EXTRASTEREO_C_TEXT N_( INDENT_SPACE2 "clipping" )
#define EXTRASTEREO_C_LONGTEXT N_( "Enable clipping. By default is enabled." )

//haas
static const int haas_middle_src_vlclist[] = { 0, 1, 2, 3 };
static const char *const haas_middle_src_vlctext[] =
{
     "left",
     "right",
     "mid",
     "side",
};

#define HAAS_TEXT N_( "haas" )
#define HAAS_LONGTEXT N_( "Haas Effect" )

#define LEVEL_IN_TEXT N_( INDENT_SPACE "level_in" )
#define LEVEL_IN_LONGTEXT N_( "Set input level. By default is 1, or 0dB" )

#define LEVEL_OUT_TEXT N_( INDENT_SPACE "level_out" )
#define LEVEL_OUT_LONGTEXT N_( "Set output level. By default is 1, or 0dB." )

#define SIDE_GAIN_TEXT N_( INDENT_SPACE "side_gain" )
#define SIDE_GAIN_LONGTEXT N_( "Set gain applied to side part of signal." \
                    " By default is 1." )

#define MIDDLE_SRC_TEXT N_( INDENT_SPACE "middle_source" )
#define MIDDLE_SRC_LONGTEXT N_( "Set kind of middle source." )

#define MIDDLE_PHASE_TEXT N_( INDENT_SPACE2 "middle_phase" )
#define MIDDLE_PHASE_LONGTEXT N_( "Change middle phase. By default is disabled." )

#define LEFT_DELAY_TEXT N_( INDENT_SPACE "left_delay" )
#define LEFT_DELAY_LONGTEXT N_( "Set left channel delay. By default is 2.05 milliseconds." )

#define LEFT_BAL_TEXT N_( INDENT_SPACE "left_balance" )
#define LEFT_BAL_LONGTEXT N_( "Set left channel balance. By default is -1." )

#define LEFT_GAIN_TEXT N_( INDENT_SPACE "left_gain" )
#define LEFT_GAIN_LONGTEXT N_( "Set left channel gain. By default is 1." )

#define LEFT_PHASE_TEXT N_( INDENT_SPACE2 "left_phase" )
#define LEFT_PHASE_LONGTEXT N_( "Change left phase. By default is disabled." )

#define RIGHT_DELAY_TEXT N_( INDENT_SPACE "right_delay" )
#define RIGHT_DELAY_LONGTEXT N_( "Set right channel delay. By default is 2.05 milliseconds." )

#define RIGHT_BAL_TEXT N_( INDENT_SPACE "right_balance" )
#define RIGHT_BAL_LONGTEXT N_( "Set right channel balance. By default is -1." )

#define RIGHT_GAIN_TEXT N_( INDENT_SPACE "right_gain" )
#define RIGHT_GAIN_LONGTEXT N_( "Set right channel gain. By default is 1." )

#define RIGHT_PHASE_TEXT N_( INDENT_SPACE2 "right_phase" )
#define RIGHT_PHASE_LONGTEXT N_( "Change right phase. By default is disabled." )

//tremolo
#define TREMOLO_TEXT N_( "tremolo" )
#define TREMOLO_LONGTEXT N_( "Tremolo: Sinusoidal amp modulation." )

#define TREMOLO_MOD_FREQUENCY_TEXT N_( INDENT_SPACE "mod frequency" )
#define TREMOLO_MOD_FREQUENCY_LONGTEXT N_( "Modulation frequency in Hertz. Modulation frequencies" \
                " in the subharmonic range (20 Hz or lower) will result in a tremolo effect." \
                " This filter may also be used as a ring modulator by specifying a modulation" \
                " frequency higher than 20 Hz. Range is 0.1 - 20000.0." \
                " Default value is 5.0 Hz." )

#define MOD_DEPTH_TEXT N_( INDENT_SPACE "mod depth" )
#define MOD_DEPTH_LONGTEXT N_( "Depth of modulation as a percentage. Range is 0.0 - 1.0." \
                " Default value is 0.5." )

//vibrato
#define VIBRATO_TEXT N_( "vibrato" )
#define VIBRATO_LONGTEXT N_( "Vibrato: Sinusoidal phase modulation." )

#define VIBRATO_MOD_FREQUENCY_TEXT N_( INDENT_SPACE "mod frequency" )
#define VIBRATO_MOD_FREQUENCY_LONGTEXT N_( "Modulation frequency in Hertz. Range is 0.1 - 20000.0." \
                " Default value is 5.0 Hz." )

#define MOD_DEPTH_TEXT N_( INDENT_SPACE "mod depth" )
#define MOD_DEPTH_LONGTEXT N_( "Depth of modulation as a percentage. Range is 0.0 - 1.0." \
                " Default value is 0.5." )
/*
//firequalizer
*/
vlc_module_begin()
    
    add_submodule()
        set_shortname( N_("avaudio_filters") )
        set_description( N_("avaudio filters") )
        set_category( CAT_AUDIO )
        set_subcategory( SUBCAT_AUDIO_AFILTER )
            
    //adelay
        add_submodule()
            add_bool( "enable-adelay", false, ADELAY_TEXT,
                        ADELAY_LONGTEXT, true )

            add_string( "adelay-delays", "0", DELAYS_TEXT,
                        DELAYS_LONGTEXT, true )                    
            
    //aecho
        add_submodule()
            add_bool( "enable-aecho", false, AECHO_TEXT,
                    AECHO_LONGTEXT, true )

            add_float_with_range( "aecho-in_gain", 0.6, 0.0, 1.0,
                    AECHO_IN_GAIN_TEXT, AECHO_IN_GAIN_LONGTEXT, false )

            add_float_with_range( "aecho-out_gain", 0.3, 0.0, 1.0,
                    AECHO_OUT_GAIN_TEXT, AECHO_OUT_GAIN_LONGTEXT, false )
            
            add_string( "delay", "1000", AECHO_DELAY_TEXT,
                        AECHO_DELAY_LONGTEXT, true )

            add_string( "decay", "0.5", AECHO_DECAY_TEXT,
                        AECHO_DECAY_LONGTEXT, true )  
    
    //anull
        add_submodule()
            add_bool( "enable-anull", false, ANULL_TEXT,
                    ANULL_LONGTEXT, true )
    
    //apulsator
        add_submodule()
            add_bool( "enable-apulsator", false, APULSATOR_TEXT,
                        APULSATOR_LONGTEXT, true )

            add_float_with_range( "apulsator-level_in", 1, 0.015625, 64,
                    APULSATOR_LEVEL_IN_TEXT, APULSATOR_LEVEL_IN_LONGTEXT, false )
            add_float_with_range( "apulsator-level_out", 1, 0.015625, 64,
                    APULSATOR_LEVEL_OUT_TEXT, APULSATOR_LEVEL_OUT_LONGTEXT, false )
            add_string( "apulsator-mode", "sine", MODE_TEXT,
                        MODE_LONGTEXT, false )
                change_string_list( apulsator_mode_list, apulsator_mode_list_text )
            add_float_with_range( "apulsator-amount", 1, 0, 1,
                        AMOUNT_TEXT, AMOUNT_LONGTEXT, false )
            add_float_with_range( "apulsator-offset_l", 0, 0, 1,
                        OFFSET_L_TEXT, OFFSET_L_LONGTEXT, false )
            add_float_with_range( "apulsator-offset_r", 0.5, 0, 1,
                        OFFSET_R_TEXT, OFFSET_R_LONGTEXT, false )
            add_float_with_range( "apulsator-width", 1, 0, 2,
                        WIDTH_TEXT, WIDTH_LONGTEXT, false )
            add_string( "apulsator-timing", "hz", TIMING_TEXT,
                        TIMING_LONGTEXT, false )
                change_string_list( apulsator_timing_list, apulsator_timing_list_text )
            add_float_with_range( "apulsator-bpm", 120, 30, 300,
                    BPM_TEXT, BPM_LONGTEXT, false )
            add_integer_with_range( "apulsator-ms", 500, 10, 2000,
                    MS_TEXT, MS_LONGTEXT, false )
            add_float_with_range( "apulsator-hz", 2, 0.1, 100,
                    HZ_TEXT, HZ_LONGTEXT, false )

    //asoftclip
        add_submodule()
            add_bool( "enable-asoftclip", false, ASOFTCLIP_TEXT,
                    ASOFTCLIP_LONGTEXT, true )
            add_string( "asoftclip-type", "exp", TYPE_TEXT,
                        TYPE_LONGTEXT, false )
                change_string_list( softclip_type_list, softclip_type_list_text )
            add_float_with_range( "asoftclip-threshold", 1, 0, 1,
                    THRESHOLD_TEXT, THRESHOLD_LONGTEXT, false )

    //chorus
        add_submodule()
            add_bool( "enable-chorus", false, CHORUS_TEXT,
                    CHORUS_LONGTEXT, true )

            add_float_with_range( "chorus-in_gain", 0.4, 0.0, 1.0,
                    IN_GAIN_TEXT, IN_GAIN_LONGTEXT, false )
            add_float_with_range( "chorus-out_gain", 0.4, 0.0, 1.0,
                    OUT_GAIN_TEXT, OUT_GAIN_LONGTEXT, false )
            
            add_string( "chorus-delay", NULL, DELAY_TEXT,
                        DELAY_LONGTEXT, true )

            add_string( "chorus-decay", NULL, DECAY_TEXT,
                        DECAY_LONGTEXT, true )

            add_string( "chorus-speed", NULL, SPEED_TEXT,
                        SPEED_LONGTEXT, true )

            add_string( "chorus-depth", NULL, DEPTH_TEXT,
                        DEPTH_LONGTEXT, true )

    //extrastereo
        add_submodule()
            add_bool( "enable-extrastereo", false, EXTRASTEREO_TEXT,
                    EXTRASTEREO_LONGTEXT, true )
            
            add_float_with_range( "extrastereo-m", 2.5, -10, 10,
                    EXTRASTEREO_M_TEXT, EXTRASTEREO_M_LONGTEXT, false )
            add_bool( "extrastereo-c", true, EXTRASTEREO_C_TEXT,
                    EXTRASTEREO_C_LONGTEXT, false )

    //haas
        add_submodule()
            add_bool( "enable-haas", false, HAAS_TEXT,
                    HAAS_LONGTEXT, true )

            add_float_with_range( "level_in", 1, 0.015625, 64,
                LEVEL_IN_TEXT, LEVEL_IN_LONGTEXT, false )
            
            add_float_with_range( "level_out", 1, 0.015625, 64,
                    LEVEL_OUT_TEXT, LEVEL_OUT_LONGTEXT, false )
            
            add_float_with_range( "side_gain", 1, 0.015625, 64,
                    SIDE_GAIN_TEXT, SIDE_GAIN_LONGTEXT, false )
            
            add_integer( "middle_source", 2,
                        MIDDLE_SRC_TEXT, NULL, true )
                change_integer_list( haas_middle_src_vlclist,
                                    haas_middle_src_vlctext )
            
            add_bool( "middle_phase", false, MIDDLE_PHASE_TEXT,
                        MIDDLE_PHASE_LONGTEXT, true )
            
            add_float_with_range( "left_delay", 2.05, 0, 40,
                    LEFT_DELAY_TEXT, LEFT_DELAY_LONGTEXT, false )
            
            add_float_with_range( "left_balance", -1, -1, 1,
                    LEFT_BAL_TEXT, LEFT_BAL_LONGTEXT, false )
            
            add_float_with_range( "left_gain", 1, 0.015625, 64,
                    LEFT_GAIN_TEXT, LEFT_GAIN_LONGTEXT, false )
            
            add_bool( "left_phase", false, LEFT_PHASE_TEXT,
                        LEFT_PHASE_LONGTEXT, true )

            add_float_with_range( "right_delay", 2.12, 0, 40,
                    RIGHT_DELAY_TEXT, RIGHT_DELAY_LONGTEXT, false )
            
            add_float_with_range( "right_balance", 1, -1, 1,
                    RIGHT_BAL_TEXT, RIGHT_BAL_LONGTEXT, false )
            
            add_float_with_range( "right_gain", 1, 0.015625, 64,
                    RIGHT_GAIN_TEXT, RIGHT_GAIN_LONGTEXT, false )
            
            add_bool( "right_phase", false, RIGHT_PHASE_TEXT,
                        RIGHT_PHASE_LONGTEXT, true )
    //tremolo
        add_submodule()
            add_bool( "enable-tremolo", false, TREMOLO_TEXT,
                    TREMOLO_LONGTEXT, true )

            add_float_with_range( "tremolo-mod-freq", 5, 0.1, 20000,
                    TREMOLO_MOD_FREQUENCY_TEXT, TREMOLO_MOD_FREQUENCY_LONGTEXT, false )
            add_float_with_range( "tremolo-mod-depth", 0.5, 0, 1,
                    MOD_DEPTH_TEXT, MOD_DEPTH_LONGTEXT, false )
            
    //vibrato
        add_submodule()
            add_bool( "enable-vibrato", false, VIBRATO_TEXT,
                    VIBRATO_LONGTEXT, true )

            add_float_with_range( "vibrato-mod-freq", 5, 0.1, 20000,
                    VIBRATO_MOD_FREQUENCY_TEXT, VIBRATO_MOD_FREQUENCY_LONGTEXT, false )
            add_float_with_range( "vibrato-mod-depth", 0.5, 0, 1,
                    MOD_DEPTH_TEXT, MOD_DEPTH_LONGTEXT, false )


    set_capability( "audio filter", 51 )
    set_callback( Open)
    add_shortcut("avaudio_filters")
//firequalizer  haas
//    add_submodule()
vlc_module_end()


/*****************************************************************************
 * Open: initialize filter
*****************************************************************************/

static int Open( vlc_object_t *p_this )
{
    bool if_adelay;
    bool if_aecho;
    bool if_anull;
    bool if_apulsator;
    bool if_asoftclip;
    bool if_chorus;
    bool if_exstrastereo;
    bool if_firequilizer;
    bool if_haas;
    bool if_tremolo;
    bool if_vibrato;

    filter_t *p_filter = (filter_t *)p_this;

    if_adelay = var_InheritBool(p_filter,"enable-adelay");
    if_aecho = var_InheritBool(p_filter,"enable-aecho");
    if_anull = var_InheritBool(p_filter,"enable-anull");
    if_apulsator = var_InheritBool(p_filter,"enable-apulsator");
    if_asoftclip = var_InheritBool(p_filter,"enable-asoftclip");
    if_chorus = var_InheritBool(p_filter,"enable-chorus");
    if_exstrastereo = var_InheritBool(p_filter,"enable-extrastereo");
    //if_firequilizer = var_InheritBool(p_filter,"enable-firequalizer");
    if_haas = var_InheritBool(p_filter,"enable-haas");
    if_tremolo = var_InheritBool(p_filter,"enable-tremolo");
    if_vibrato = var_InheritBool(p_filter,"enable-vibrato");
printf("Inside Open for callback\n\n");

    if(if_adelay)
        Open_adelay(p_this);
    if(if_aecho)
        Open_aecho(p_this);
    if(if_anull)
        Open_anull(p_this);
    if(if_apulsator)
        Open_apulsator(p_this);
    if(if_asoftclip)
        Open_asoftclip(p_this);
    if(if_chorus)
        Open_chorus(p_this);
    if(if_exstrastereo)
        Open_extrastereo(p_this);
    if(if_haas)
        Open_haas(p_this);
    if(if_tremolo)
        Open_tremolo(p_this);
    if(if_vibrato)
        Open_vibrato(p_this);
    
    if((!if_adelay && !if_aecho && !if_anull && !if_apulsator && !if_asoftclip 
        && !if_chorus && !if_exstrastereo && !if_haas && !if_firequilizer && 
        !if_tremolo &&!if_vibrato ))
        Open_anull(p_this);
    
    p_filter->fmt_out.audio = p_filter->fmt_in.audio;

    return VLC_SUCCESS;
}

static int Open_adelay( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    char *adelay_delay;
    
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_filter->p_sys = p_sys;
    adelay_delay = var_InheritString(vlc_object_parent(p_filter),"adelay-delays");
    

/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "adelay=");

    /* Checking delay Bounds */
    if (adelay_delay != NULL)
    {
        char *delay_all = malloc(200 * sizeof(delay_all));
        delay_all = check_Bounds(p_filter, adelay_delay, -0.000001, 90000, 0);

        vlc_memstream_printf(&ms, "%s", delay_all);
        free(delay_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "0");
        msg_Warn(p_filter, "delay parameter not set. Set to default(0).\n");
    }


    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;


    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;


    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    free(ms.ptr);

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_aecho( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    float aecho_in_gain;
    float aecho_out_gain;
    char *aecho_delay;
    char *aecho_decay;
    
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;


    p_filter->p_sys = p_sys;
    aecho_in_gain = var_InheritFloat(vlc_object_parent(p_filter),"aecho-in_gain");
    aecho_out_gain = var_InheritFloat(p_filter,"aecho-out_gain");
    aecho_delay = var_InheritString(p_filter,"delay");
    aecho_decay = var_InheritString(p_filter,"decay");
    

/** Configure filter parameters (filter_desc)**/

    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "aecho=%f:%f:",aecho_in_gain, aecho_out_gain);

    /* Checking delay Bounds   */
    if (aecho_decay != NULL)
    {
        char *delay_all = malloc(200 * sizeof(delay_all));
        delay_all = check_Bounds(p_filter, aecho_delay, 0, 90000, 1000);

        vlc_memstream_printf(&ms, "%s:", delay_all);
        free(delay_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "1000:");
        msg_Warn(p_filter, "delay parameter not set. Set to default(1000).\n");
    }

    /* Checking decay Bounds   */
    if (aecho_decay != NULL)
    {
        char *decay_all = malloc(200 * sizeof(decay_all));
        decay_all = check_Bounds(p_filter, aecho_decay, 0, 1, 0.5);

        vlc_memstream_printf(&ms, "%s", decay_all);
        free(decay_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "0.5");
        msg_Warn(p_filter, "decay parameter not set. Set to default(0.5).\n");
    }

    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;
    
    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);

    
    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    
    
    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }
    free( ms.ptr );

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_anull( vlc_object_t *p_this )
{
    struct vlc_memstream ms;
    
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_filter->p_sys = p_sys;

/** Configure filter parameters (filter_desc)**/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "anull");
    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;
    

    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);
    
    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    
    
    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }
    
    free( ms.ptr );

    frame_len_flag = 0;

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_apulsator( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    float apulsator_level_in;
    float apulsator_level_out;
    char* apulsator_mode;
    float apulsator_amount;
    float apulsator_offset_l;
    float apulsator_offset_r;
    float apulsator_width;
    char* apulsator_timing;
    float apulsator_bpm;
    int apulsator_ms;
    float apulsator_hz;

    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;


    p_filter->p_sys = p_sys;
    apulsator_level_in = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-level_in");
    apulsator_level_out = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-level_out");
    apulsator_mode = var_InheritString(vlc_object_parent(p_filter),"apulsator-mode");
    apulsator_amount = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-amount");
    apulsator_offset_l = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-offset_l");
    apulsator_offset_r = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-offset_r");
    apulsator_width = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-width");
    apulsator_timing = var_InheritString(vlc_object_parent(p_filter),"apulsator-timing");
    apulsator_bpm = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-bpm");
    apulsator_ms = var_InheritInteger(vlc_object_parent(p_filter),"apulsator-ms");
    apulsator_hz = var_InheritFloat(vlc_object_parent(p_filter),"apulsator-hz");

    
/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;
    
    vlc_memstream_printf(&ms, "apulsator=%f:%f:", apulsator_level_in, apulsator_level_out);
    vlc_memstream_printf(&ms, "%s:", apulsator_mode);
    vlc_memstream_printf(&ms, "%f:", apulsator_amount);
    vlc_memstream_printf(&ms, "%f:", apulsator_offset_l);
    vlc_memstream_printf(&ms, "%f:", apulsator_offset_r);
    vlc_memstream_printf(&ms, "%f:", apulsator_width);
    vlc_memstream_printf(&ms, "%s=", apulsator_timing);

    if (!strcmp(apulsator_timing, "bpm"))
        vlc_memstream_printf(&ms, "%f", apulsator_bpm);
    else if (!strcmp(apulsator_timing, "ms"))
        vlc_memstream_printf(&ms, "%d", apulsator_ms);
    else
        vlc_memstream_printf(&ms, "%f", apulsator_hz);


    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;


    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);


    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    
    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    free(ms.ptr);

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_asoftclip( vlc_object_t *p_this )
{
    struct vlc_memstream ms;
    
    char* asoftclip_type;
    float asoftclip_threshold;
    
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_filter->p_sys = p_sys;
    asoftclip_type = var_InheritString(p_filter,"asoftclip-type");
    asoftclip_threshold = var_InheritFloat(vlc_object_parent(p_filter),"asoftclip-threshold");
    
/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "asoftclip=type=%s:%f", asoftclip_type, asoftclip_threshold);
    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;
        
    
    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    
    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    free(ms.ptr);

    frame_len_flag = 0;

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_chorus( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    float chorus_in_gain;
    float chorus_out_gain;
    char *chorus_delay;
    char *chorus_decay;
    char *chorus_speed;
    char *chorus_depth;

    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;


    p_filter->p_sys = p_sys;
    
    chorus_in_gain = var_InheritFloat(vlc_object_parent(p_filter),"chorus-in_gain");
    chorus_out_gain = var_InheritFloat(p_filter,"chorus-out_gain");
    chorus_delay = var_InheritString(p_filter,"chorus-delay");
    chorus_decay = var_InheritString(p_filter,"chorus-decay");
    chorus_speed = var_InheritString(p_filter,"chorus-speed");
    chorus_depth = var_InheritString(p_filter,"chorus-depth");
    

/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "chorus=%f:%f:", chorus_in_gain, chorus_out_gain);

    /* Checking delay Bounds */   
    if (chorus_delay != NULL)
    {
        char *delay_all = malloc(200 * sizeof(delay_all));
        delay_all = check_Bounds(p_filter, chorus_delay, 0, 90000, 1000);

        vlc_memstream_printf(&ms, "%s:", delay_all);
        free(delay_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "1000:");
        msg_Warn(p_filter, "delay parameter not set. Set to default(1000).\n");
    }

    /* Checking decay Bounds */   
    if (chorus_decay != NULL)
    {
        char *decay_all = malloc(200 * sizeof(decay_all));
        decay_all = check_Bounds(p_filter, chorus_decay, 0, 1, 0.5);

        vlc_memstream_printf(&ms, "%s:", decay_all); 
        free(decay_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "0.5:");
        msg_Warn(p_filter, "decay parameter not set. Set to default(0.5).\n");
    }
    
    /* Checking speed Bounds */
    if (chorus_speed != NULL)
    {
        char *speed_all = malloc(200 * sizeof(speed_all));
        speed_all = check_Bounds(p_filter, chorus_speed, 0, 1, 0.5);

        //strcat(filter_descr, speed_all);
        vlc_memstream_printf(&ms, "%s:", speed_all);
        free(speed_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "0.5:");
        msg_Warn(p_filter, "speed parameter not set. Set to default(0.5).\n");
    }

    /* Checking depth Bounds */
    if (chorus_depth != NULL)
    {
        char *depth_all = malloc(200 * sizeof(depth_all));
        depth_all = check_Bounds(p_filter, chorus_depth, 0, 10, 2);

        vlc_memstream_printf(&ms, "%s", depth_all);
        free(depth_all);
    }
    else
    {
        vlc_memstream_printf(&ms, "2");
        msg_Warn(p_filter, "depth parameter not set. Set to default(2).\n");
    }

    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;

    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    

    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    free( ms.ptr );

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_extrastereo( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    float extrastereo_m;
    bool extrastereo_c;
    
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;


    p_filter->p_sys = p_sys;
    extrastereo_m = var_InheritFloat(vlc_object_parent(p_filter),"extrastereo-m");
    extrastereo_c = var_InheritBool(vlc_object_parent(p_filter),"extrastereo-c");
    

/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;

    vlc_memstream_printf(&ms, "extrastereo=m=%f:", extrastereo_m);

    if(extrastereo_c)
        vlc_memstream_printf(&ms, "c=1");
    else
        vlc_memstream_printf(&ms, "c=0");


    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;

    
    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;


    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    free(ms.ptr);

    frame_len_flag = 0;

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_haas( vlc_object_t *p_this )
{
    struct vlc_memstream ms;

    float haas_level_in;
    float haas_level_out;
    float haas_side_gain;
    int haas_middle_src;
    bool haas_mid_phase;
    float haas_left_delay;
    float haas_left_bal;
    float haas_left_gain;
    bool haas_left_phase;
    float haas_right_delay;
    float haas_right_bal;
    float haas_right_gain;
    bool haas_right_phase;

    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_filter->p_sys = p_sys;

    haas_level_in = var_InheritFloat(vlc_object_parent(p_filter),"level_in");
    haas_level_out = var_InheritFloat(p_filter,"level_out");
    haas_side_gain = var_InheritFloat(p_filter,"side_gain");
    haas_middle_src = var_InheritInteger(p_filter,"middle_source");
    haas_mid_phase = var_InheritBool(p_filter,"middle_phase");
    haas_left_delay = var_InheritFloat(p_filter,"left_delay");
    haas_left_bal = var_InheritFloat(p_filter,"left_balance");
    haas_left_gain = var_InheritFloat(p_filter,"left_gain");
    haas_left_phase = var_InheritBool(p_filter,"left_phase");
    haas_right_delay = var_InheritFloat(p_filter,"right_delay");
    haas_right_bal = var_InheritFloat(p_filter,"right_balance");
    haas_right_gain = var_InheritFloat(p_filter,"right_gain");
    haas_right_phase = var_InheritBool(p_filter,"right_phase");
    

/** Configure filter parameters (filters_descr) **/
    if( vlc_memstream_open( &ms ) != 0 )
        return VLC_EGENERIC;
    
    vlc_memstream_printf(&ms, "haas=%f:%f:%f:", haas_level_in, haas_level_out, haas_side_gain);

    switch (haas_middle_src)
    {
    case 0:
        vlc_memstream_printf(&ms, "left:");
        break;
    case 1:
        vlc_memstream_printf(&ms, "right:");
        break;
    case 2:
        vlc_memstream_printf(&ms, "mid:");

        break;
    case 3:
        vlc_memstream_printf(&ms, "side:");
        break;
    default:
        break;
    }
    
    vlc_memstream_printf(&ms, "middle_phase=");
    if(haas_mid_phase)
        vlc_memstream_printf(&ms, "1:");
    else
        vlc_memstream_printf(&ms, "0:");

    vlc_memstream_printf(&ms, "left_delay=");
    vlc_memstream_printf(&ms, "%f:", haas_left_delay);
    vlc_memstream_printf(&ms, "left_balance=");
    vlc_memstream_printf(&ms, "%f:", haas_left_bal);
    vlc_memstream_printf(&ms, "left_gain=");
    vlc_memstream_printf(&ms, "%f:", haas_left_gain);
    vlc_memstream_printf(&ms, "left_phase=");
    if(haas_left_phase)
        vlc_memstream_printf(&ms, "1:");
    else
        vlc_memstream_printf(&ms, "0:");
    
    vlc_memstream_printf(&ms, "right_delay=");
    vlc_memstream_printf(&ms, "%f:", haas_right_delay);
    vlc_memstream_printf(&ms, "right_balance=");
    vlc_memstream_printf(&ms, "%f:", haas_right_bal);
    vlc_memstream_printf(&ms, "right_gain=");
    vlc_memstream_printf(&ms, "%f:", haas_right_gain);
    vlc_memstream_printf(&ms, "right_phase=");
    if(haas_right_phase)
        vlc_memstream_printf(&ms, "1");
    else
        vlc_memstream_printf(&ms, "0");
 

    if(vlc_memstream_close(&ms))
        return VLC_EGENERIC;
    
    /*filter description*/
    msg_Info(p_filter,"filter_descr:%s\n",ms.ptr);


    p_filter->fmt_out.audio = p_filter->fmt_in.audio;
    
    /* initialize ffmpeg filter */
    if(init_filters(ms.ptr,p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }
    
    free(ms.ptr);

    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_tremolo( vlc_object_t *p_this )
{   
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_sys_tremolo = malloc( sizeof( *p_sys ) );

    p_filter->p_sys = p_sys;
    vlc_object_t *p_aout = vlc_object_parent(p_filter);
    p_sys_tremolo->tremolo_mfreq = var_CreateGetFloat(vlc_object_parent(p_filter),"tremolo-mod-freq");
    p_sys_tremolo->tremolo_mdepth = var_CreateGetFloat(vlc_object_parent(p_filter),"tremolo-mod-depth");
    
    var_AddCallback( p_aout, "tremolo-mod-freq", paramCallback_tremolo, p_sys_tremolo );
    var_AddCallback( p_aout, "tremolo-mod-depth", paramCallback_tremolo, p_sys_tremolo );

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;


    /* initialize ffmpeg filter */
    if(init_filters(string_generate_tremolo(p_filter),p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }


    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int Open_vibrato( vlc_object_t *p_this )
{
    filter_t *p_filter = (filter_t *)p_this;
    filter_sys_t *p_sys = vlc_object_create( p_this, sizeof( *p_sys ) );
    if( unlikely( p_sys == NULL ) )
        return VLC_ENOMEM;

    p_sys_vibrato = malloc( sizeof( *p_sys ) );
    
    p_filter->p_sys = p_sys;
    vlc_object_t *p_aout = vlc_object_parent(p_filter);
    p_sys_vibrato->vibrato_mfreq = var_CreateGetFloat(vlc_object_parent(p_filter),"vibrato-mod-freq");
    p_sys_vibrato->vibrato_mdepth = var_CreateGetFloat(vlc_object_parent(p_filter),"vibrato-mod-depth");

    var_AddCallback( p_aout, "vibrato-mod-freq",  paramCallback_vibrato, p_sys_vibrato );
    var_AddCallback( p_aout, "vibrato-mod-depth", paramCallback_vibrato, p_sys_vibrato );

    p_filter->fmt_out.audio = p_filter->fmt_in.audio;


    /* initialize ffmpeg filter */
    if(init_filters(string_generate_vibrato(p_filter),p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }


    static const struct vlc_filter_operations filter_ops =
        { .filter_audio = Process, .close = Close };
    p_filter->ops = &filter_ops;

    return VLC_SUCCESS;
}

static int paramCallback_tremolo( vlc_object_t *p_this, char const *psz_cmd,
                            vlc_value_t oldval, vlc_value_t newval,
                            void * p_data )
{
    filter_t* p_filter =(filter_t*)p_this;
    filter_sys_t *p_sys = p_filter->p_sys;
printf("    Inside tremolo callback\n");
    vlc_mutex_lock(&callback_lock);
    
    p_sys_tremolo = p_data;
    VLC_UNUSED(psz_cmd); VLC_UNUSED(oldval);
    
    vlc_mutex_lock( &p_sys_tremolo->lock );
    
    if( !strcmp( psz_cmd, "tremolo-mod-freq" ) )
        p_sys_tremolo->tremolo_mfreq = newval.f_float;
    if( !strcmp( psz_cmd, "tremolo-mod-depth" ) )
        p_sys_tremolo->tremolo_mdepth = newval.f_float;

    /* initialize ffmpeg filter */
    if(init_filters(string_generate_tremolo(p_filter),p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }

    vlc_mutex_unlock( &p_sys_tremolo->lock );
    vlc_mutex_unlock( &callback_lock);

    return VLC_SUCCESS;
}
static int paramCallback_vibrato( vlc_object_t *p_this, char const *psz_cmd,
                            vlc_value_t oldval, vlc_value_t newval,
                            void * p_data )
{
    filter_t* p_filter =(filter_t*)p_this;
    filter_sys_t *p_sys = p_filter->p_sys;
printf("    Inside vibrato callback\n");

    vlc_mutex_lock(&callback_lock);

    p_sys_tremolo = p_data;
    VLC_UNUSED(psz_cmd); VLC_UNUSED(oldval);
    
    vlc_mutex_lock( &p_sys_tremolo->lock );
    
    if( !strcmp( psz_cmd, "vibrato-mod-freq" ) )
        p_sys_vibrato->vibrato_mfreq = newval.f_float;
    if( !strcmp( psz_cmd, "vibrato-mod-depth" ) )
        p_sys_vibrato->vibrato_mdepth = newval.f_float;

    /* initialize ffmpeg filter */
    if(init_filters(string_generate_vibrato(p_filter),p_filter) <0)
    {
        avfilter_graph_free(&p_sys->filter_graph);
        free(p_sys);
        return VLC_EGENERIC;
    }
    
    vlc_mutex_unlock( &p_sys_tremolo->lock );
    vlc_mutex_unlock( &callback_lock);

    return VLC_SUCCESS;
}

static char* string_generate_tremolo(filter_t* p_filter)
{
    struct vlc_memstream ms;

    if( vlc_memstream_open( &ms ) != 0 )
        return NULL;

    vlc_memstream_printf(&ms, "tremolo=%f:%f", p_sys_tremolo->tremolo_mfreq, p_sys_tremolo->tremolo_mdepth);

     if(vlc_memstream_close(&ms))
        return NULL;
    
    /*filter description*/
    msg_Dbg(p_filter,"filter_descr:%s\n",ms.ptr);

    return ms.ptr;
}
static char* string_generate_vibrato(filter_t* p_filter)
{
    struct vlc_memstream ms;

    if( vlc_memstream_open( &ms ) != 0 )
        return NULL;

    vlc_memstream_printf(&ms, "vibrato=%f:%f", p_sys_vibrato->vibrato_mfreq, p_sys_vibrato->vibrato_mdepth);

     if(vlc_memstream_close(&ms))
        return NULL;

    /*filter description*/
    msg_Dbg(p_filter,"filter_descr:%s\n",ms.ptr);

    return ms.ptr;
}